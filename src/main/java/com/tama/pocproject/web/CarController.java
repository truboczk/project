package com.tama.pocproject.web;

import com.tama.pocproject.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "car")
@CacheConfig(cacheNames={"userData"})
public class CarController {

    private final EmailService emailService;

    @GetMapping("/{id}")
    @Cacheable
    public ResponseEntity<String> retrieveUserData(@PathVariable Integer id) {

        return new ResponseEntity<>(emailService.sendEmail(id), HttpStatus.OK);
    }

}
