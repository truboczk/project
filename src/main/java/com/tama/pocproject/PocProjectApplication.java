package com.tama.pocproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class PocProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocProjectApplication.class, args);
	}

}
// todo readme
// todo javadoc
// todo api doc
