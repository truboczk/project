package com.tama.pocproject.service;

import com.tama.pocproject.persistence.entity.CarEntity;
import com.tama.pocproject.persistence.entity.CarsOfPeopleEntity;
import com.tama.pocproject.persistence.entity.PersonDataEntity;
import com.tama.pocproject.persistence.repository.CarsOfPeopleRepository;
import com.tama.pocproject.persistence.repository.PersonDataRepository;
import com.tama.pocproject.service.validation.ProcessingValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final PersonDataRepository personDataRepository;
    private final CarsOfPeopleRepository carsOfPeopleRepository;
    private final ProcessingValidator processingValidator;
    private final TemplateResolverService templateResolverService;

    public String sendEmail(Integer personId) {

        Optional<PersonDataEntity> personDataEntity = personDataRepository.findById(personId);
        List<CarsOfPeopleEntity> cars = carsOfPeopleRepository.findByPersonId(personId);

        if(personDataEntity.isPresent()) {
            PersonDataEntity personDataEntityExtracted = personDataEntity.get();

            List<CarEntity> carEntities = cars.stream()
                    .map(carsOfPeopleEntity -> carsOfPeopleEntity.getCarEntity())
                    .filter(carEntity -> processingValidator.validate(carEntity))
                    .collect(Collectors.toList());

            //todo empty list check
            return templateResolverService.resolve(carEntities, personDataEntityExtracted);
        }

        return null;
    }

}
