package com.tama.pocproject.service;

import com.tama.pocproject.persistence.entity.CarEntity;
import com.tama.pocproject.persistence.entity.PersonDataEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateResolverService {

    public static final String CARS_LOOP_END = "<carsLoopEnd>";
    public static final String CARS_LOOP_BEGIN = "<carsLoopBegin>";
    public static final String NAME = "<name>";
    public static final String COUNTRY = "<country>";
    public static final String DATE_OF_BIRTH = "<dateOfBirth>";
    public static final String BRAND = "<brand>";
    public static final String TYPE = "<type>";
    public static final String PLATE_NUMBER = "<plateNumber>";
    public static final String YEAR_OF_MANUFACTURE = "<yearOfManufacture>";
    public static final String DRIVEN_DISTANCE = "<drivenDistance>";
    public static final String CALCULATED_VALUE = "<calculatedValue>";

    public String resolve(List<CarEntity> carEntities, PersonDataEntity personDataEntity) {

        StringBuilder finalMessage = new StringBuilder();
        String text = personDataEntity.getEmailTemplatesEntity().getText();

        int startIndexOfCarTemplate = text.indexOf(CARS_LOOP_BEGIN);
        int endIndexOfCarTemplate = text.indexOf(CARS_LOOP_END) + CARS_LOOP_END.length();

        String startTemplate = text.substring(0, startIndexOfCarTemplate);
        String carTemplate = text.substring(startIndexOfCarTemplate, endIndexOfCarTemplate);
        String endTemplate = text.substring(endIndexOfCarTemplate, text.length()-1);

        createHeader(personDataEntity, finalMessage, startTemplate);
        createCarsPart(carEntities, finalMessage, carTemplate);
        finalMessage.append(endTemplate);

        return getFinalResultString(finalMessage);
    }

    private String getFinalResultString(StringBuilder finalMessage) {
        String finalMessageString = finalMessage.toString();

        finalMessageString = finalMessageString.replace(CARS_LOOP_BEGIN, "");
        finalMessageString = finalMessageString.replace(CARS_LOOP_END, "");

        return finalMessageString;
    }

    private void createCarsPart(List<CarEntity> carEntities, StringBuilder finalMessage, String carTemplate) {
        for(CarEntity carEntity : carEntities) {
            String carDetails= carTemplate;

            carDetails = carDetails.replace(BRAND, carEntity.getBrand());
            carDetails = carDetails.replace(TYPE, carEntity.getType());
            carDetails = carDetails.replace(PLATE_NUMBER, carEntity.getPlateNumber());
            carDetails = carDetails.replace(YEAR_OF_MANUFACTURE, carEntity.getYearOfManufacture().toString());
            carDetails = carDetails.replace(DRIVEN_DISTANCE, carEntity.getDrivenDistance().toString());
            carDetails = carDetails.replace(CALCULATED_VALUE, carEntity.getCalculatedValue().toString());

            finalMessage.append(carDetails);
        }
    }

    private void createHeader(PersonDataEntity personDataEntity, StringBuilder finalMessage, String startTemplate) {
        startTemplate = startTemplate.replace(NAME, personDataEntity.getName());
        startTemplate = startTemplate.replace(COUNTRY, personDataEntity.getCountry());
        startTemplate = startTemplate.replace(DATE_OF_BIRTH, personDataEntity.getDataOfBirht().toString());

        finalMessage.append(startTemplate);
    }

}
