package com.tama.pocproject.service.validation;


import com.tama.pocproject.persistence.entity.CarEntity;
import org.springframework.stereotype.Component;

@Component
public class ProcessingValidator {

    public boolean validate(CarEntity carEntity) {

        if (carEntity.getCalculatedValue() > 0 && !carEntity.getIsSent()) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }
}
