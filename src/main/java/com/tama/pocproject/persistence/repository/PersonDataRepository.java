package com.tama.pocproject.persistence.repository;

import com.tama.pocproject.persistence.entity.PersonDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonDataRepository extends JpaRepository<PersonDataEntity, Integer> {
}
