package com.tama.pocproject.persistence.repository;

import com.tama.pocproject.persistence.entity.CarsOfPeopleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CarsOfPeopleRepository extends JpaRepository<CarsOfPeopleEntity, Integer> {
    //todo generation based on name
    @Query("SELECT c FROM CarsOfPeopleEntity c WHERE c.personDataEntity.personId =:personId")
    List<CarsOfPeopleEntity> findByPersonId(Integer personId);
}
