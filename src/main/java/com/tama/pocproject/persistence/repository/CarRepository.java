package com.tama.pocproject.persistence.repository;

import com.tama.pocproject.persistence.entity.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<CarEntity, Integer>{
}
