package com.tama.pocproject.persistence.repository;

import com.tama.pocproject.persistence.entity.EmailTemplatesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailTemplatesRepository extends JpaRepository<EmailTemplatesEntity, Integer> {
}
