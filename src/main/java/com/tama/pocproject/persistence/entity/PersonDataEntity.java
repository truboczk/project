package com.tama.pocproject.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "person_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="person_id")
    private Integer personId;

    private String name;

    @Column(name="data_of_birht")
    private LocalDate dataOfBirht;

    private String country;

    @ManyToOne
    @JoinColumn(name = "language_id")
    private EmailTemplatesEntity emailTemplatesEntity;
}
