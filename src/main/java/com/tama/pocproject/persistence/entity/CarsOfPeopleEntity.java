package com.tama.pocproject.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cars_of_people")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarsOfPeopleEntity {

    @EmbeddedId
    private CarsOfPeopleId id;

    @MapsId("car_id")
    @JoinColumn(name = "car_id")
    @ManyToOne
    private CarEntity carEntity;

    @MapsId("person_id")
    @JoinColumn(name = "person_id")
    @ManyToOne
    private PersonDataEntity personDataEntity;


}
