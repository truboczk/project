package com.tama.pocproject.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cars")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="car_id")
    private Integer carId;

    private String brand;

    private String type;

    @Column(name="plate_number")
    private String plateNumber;

    @Column(name="year_of_manufacture")
    private Integer yearOfManufacture;

    @Column(name="calculated_value")
    private Integer calculatedValue;

    @Column(name="driven_distance")
    private Integer drivenDistance;

    @Column(name="is_sent")
    private Boolean isSent;

}
