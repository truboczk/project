package com.tama.pocproject.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarsOfPeopleId implements Serializable {
    @Column(name="car_id")
    Integer carId;

    @Column(name="person_id")
    Integer personId;
}
