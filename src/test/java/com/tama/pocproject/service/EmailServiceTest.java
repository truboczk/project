package com.tama.pocproject.service;

import com.tama.pocproject.persistence.entity.CarEntity;
import com.tama.pocproject.persistence.entity.CarsOfPeopleEntity;
import com.tama.pocproject.persistence.entity.PersonDataEntity;
import com.tama.pocproject.persistence.repository.CarsOfPeopleRepository;
import com.tama.pocproject.persistence.repository.PersonDataRepository;
import com.tama.pocproject.service.validation.ProcessingValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {

    private static final Integer PERSON_ID = 1;

    @InjectMocks
    private EmailService emailService;

    @Mock
    private PersonDataRepository personDataRepository;

    @Mock
    private CarsOfPeopleRepository carsOfPeopleRepository;

    @Mock
    private ProcessingValidator processingValidator;

    @Mock
    private TemplateResolverService templateResolverService;

    @Test
    public void sendEmailTest() {
        //Given
        final PersonDataEntity personDataEntity = new PersonDataEntity();
        final CarEntity carEntity = new CarEntity();
        final CarsOfPeopleEntity e1 = CarsOfPeopleEntity.builder().carEntity(carEntity).build();
        final String expected = "expected";

        when(personDataRepository.findById(PERSON_ID)).thenReturn(Optional.of(personDataEntity));
        when(carsOfPeopleRepository.findByPersonId(PERSON_ID)).thenReturn(List.of(e1, e1));
        when(processingValidator.validate(carEntity)).thenReturn(Boolean.TRUE);
        when(templateResolverService.resolve(List.of(e1.getCarEntity(), e1.getCarEntity()), personDataEntity)).thenReturn(expected);

        //When
        String actualResult = emailService.sendEmail(PERSON_ID);

        //Then
        assertEquals(expected, actualResult);
    }

    @Test
    public void sendEmailTestWithoutPerson() {
        //Given
        when(personDataRepository.findById(PERSON_ID)).thenReturn(Optional.empty());

        //When
        String actualResult = emailService.sendEmail(PERSON_ID);

        //Then
        assertNull(actualResult);
    }

}
