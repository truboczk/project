package com.tama.pocproject.integrationtest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.FileCopyUtils;

import java.io.InputStreamReader;
import java.io.Reader;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CarControllerIntegrationTest {

    private static final String EXPECTED_RESPONSE_TXT = "expectedResponse.txt";
    private static final String URL = "/car/{id}";

    @Autowired
    private MockMvc mockMvc;

    @Value("classpath:" + EXPECTED_RESPONSE_TXT)
    private Resource expectedText;

    @Test
    public void testRetrieveDog() throws Exception {
        String contentAsString = mockMvc.perform(get(URL, 1)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        Reader reader = new InputStreamReader(expectedText.getInputStream(), UTF_8);

        String s = FileCopyUtils.copyToString(reader);

        assertEquals(s, contentAsString);
    }


}
